import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from '../components/views/home';
import iD from '../components/account/iD';
import Point from '../components/account/point';
import Cert from '../components/account/cert';
import Share from '../components/account/share';
import GroupsScreen from '../components/chat/GroupsScreen';
import ChatScreen from '../components/chat/ChatScreen';
import AddGroupScreen from '../components/chat/AddGroupScreen';

const Stack = createNativeStackNavigator();

const screenOptionStyle = {
  headerStyle: {
    backgroundColor: '#ffff',
  },
  gestureDirection:'horizontal-inverted',
  animationEnabled: false,
};

const HomeStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

const AccountStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="iD" component={iD} options={{headerShown: false}} />
      <Stack.Screen
        name="Point"
        component={Point}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Cert"
        component={Cert}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Share"
        component={Share}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

const ChatStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen
        name="Groups Screen"
        component={GroupsScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Chat Screen"
        component={ChatScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Add Group Screen"
        component={AddGroupScreen}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export {HomeStackNavigator, AccountStackNavigator, ChatStackNavigator};
