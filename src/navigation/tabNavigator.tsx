import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import ChatScreen from '../components/chat/ChatScreen';
import GroupsScreen from '../components/chat/GroupsScreen';

import News from '../components/news/news';
import Legis from '../components/news/legis';
import Mag from '../components/news/mag';
import HeaderView from '../common/headerView';

import Bene from '../components/benefits/bene';
import Promo from '../components/benefits/promo';
import Caravan from '../components/benefits/caravan';
import AddGroupScreen from '../components/chat/AddGroupScreen';

const Tab = createBottomTabNavigator();

const ChatsTabNavigator = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Groups Screen"
        component={GroupsScreen}
        options={{ title: "Groups" , headerShown: false}}
      />
      <Tab.Screen
        name="Chat Screen"
        component={ChatScreen}
        options={{ title: "Chats", headerShown: false }}
      />
      <Tab.Screen
        name="Add Group Screen"
        component={AddGroupScreen}
        options={{ title: "Add Group" }}
      />
    </Tab.Navigator>
  );
};

const NewsTabNavigator = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Legis" component={Legis} options={{headerShown: false}}/>
      <Tab.Screen name="News" component={News} options={{headerShown: false}} />
      <Tab.Screen name="Mag" component={Mag} options={{headerShown: false}} />
    </Tab.Navigator>
  );
};

const BeneTabNavigator = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Bene" component={Bene} options={{headerShown: false}} />
      <Tab.Screen
        name="Promo"
        component={Promo}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name="Caravan"
        component={Caravan}
        options={{headerShown: false}}
      />
    </Tab.Navigator>
  );
};

export {ChatsTabNavigator, NewsTabNavigator, BeneTabNavigator};
