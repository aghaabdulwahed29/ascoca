import React from 'react';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/Feather';
import IconM from 'react-native-vector-icons/MaterialCommunityIcons';
import {HomeStackNavigator, AccountStackNavigator, ChatStackNavigator} from './StackNavigator';
import Login from '../components/views/login';
import Last from '../components/views/last';
import Train from '../components/views/train';
import Market from '../components/views/market';
import Info from '../components/settings/info';
import Notif from '../components/settings/notif';
import {ChatsTabNavigator} from './tabNavigator';
import {NewsTabNavigator} from './tabNavigator';
import {BeneTabNavigator} from './tabNavigator';
import {View, Text} from 'react-native';
import HeaderHome from '../common/headerHome';
import HeaderView from '../common/headerView';

const Drawer = createDrawerNavigator();

const CustomDrawer = (props: any) => {
  return (
    <View style={{flex: 1}}>
      <DrawerContentScrollView {...props}>
        <View
          style={{
            padding: 20,
            paddingBottom: 0,
          }}>
          <View>
            <Text
              style={{
                fontSize: 40,
                fontFamily: 'Nunito-Bold',
              }}>
              Menú
            </Text>
          </View>
        </View>
        <DrawerItemList {...props} />
      </DrawerContentScrollView>
    </View>
  );
};

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator
      initialRouteName="Login"
      screenOptions={{
        headerShown: true,
        headerStyle: {
          elevation: 0,
          shadowOpacity: 0,
        },
        headerTitle: '',
      }}
      drawerContent={props => <CustomDrawer {...props} />}>
      <Drawer.Screen
        name="Login"
        component={Login}
        options={{
          drawerLabel: () => null,
          headerShown: false,
        }}
      />
      <Drawer.Screen
        name="Inicio"
        component={HomeStackNavigator}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 22,
            color: '#000',
          },
          drawerLabel: 'Inicio',
          drawerIcon: () => <Icon name="home" size={30} color="#C10202" />,
          headerTitle: () => <HeaderHome />,
        }}
      />
      <Drawer.Screen
        name="Account"
        component={AccountStackNavigator}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 22,
            color: '#000',
          },
          drawerLabel: 'Mi cuenta',
          drawerIcon: () => <Icon name="user" size={30} color="#C10202" />,
          headerTitle: () => <HeaderView />,
        }}
      />
      <Drawer.Screen
        name="Last"
        component={Last}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 22,
            color: '#000',
          },
          drawerLabel: 'Último Minuto',
          drawerIcon: () => <Icon name="mail" size={30} color="#C10202" />,
        }}
      />
      <Drawer.Screen
        name="Chats"
        component={ChatStackNavigator}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 22,
            color: '#000',
          },
          drawerLabel: 'Chats',
          drawerIcon: () => (
            <Icon name="message-square" size={30} color="#C10202" />
          ),
          headerTitle: () => <HeaderView />,
        }}
      />
      <Drawer.Screen
        name="News"
        component={NewsTabNavigator}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 22,
            color: '#000',
          },
          drawerLabel: 'Noticias de Pequeño Comercio',
          drawerIcon: () => <Icon name="calendar" size={30} color="#C10202" />,
          headerTitle: () => <HeaderView />,
        }}
      />
      <Drawer.Screen
        name="Train"
        component={Train}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 22,
            color: '#000',
          },
          drawerLabel: 'Capacitación',
          drawerIcon: () => <Icon name="award" size={30} color="#C10202" />,
          headerTitle: () => <HeaderView />,
        }}
      />
      <Drawer.Screen
        name="Bene"
        component={BeneTabNavigator}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 22,
            color: '#000',
          },
          drawerLabel: 'Beneficios',
          drawerIcon: () => (
            <Icon name="credit-card" size={30} color="#C10202" />
          ),
          headerTitle: () => <HeaderView />,
        }}
      />
      <Drawer.Screen
        name="Market"
        component={Market}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 22,
            color: '#000',
          },
          drawerLabel: 'Mercado',
          drawerIcon: () => (
            <IconM name="comment-text-outline" size={30} color="#C10202" />
          ),
        }}
      />
      <Drawer.Screen
        name="Inf"
        component={Info}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 22,
            color: '#000',
          },
          drawerLabel: 'Quiénes Somos',
          drawerIcon: () => <Icon name="users" size={30} color="#C10202" />,
           headerTitle: () => <HeaderView />,
        }}
      />
      <Drawer.Screen
        name="Notif"
        component={Notif}
        options={{
          drawerLabelStyle: {
            fontFamily: 'Nunito-Bold',
            fontSize: 22,
            color: '#000',
          },
          drawerLabel: 'Configuración',
          drawerIcon: () => <Icon name="settings" size={30} color="#C10202" />,
          headerTitle: () => <HeaderView />,
        }}
      />
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;
