export interface IMessage {
    id: number;
    text: string;
    timecreated: number;
    conversationid?: number;
    useridfrom: number;
    candeletemessagesforallusers?: boolean;
}