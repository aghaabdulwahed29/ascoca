import { IMember } from "./member-interface";
import { IMessage } from "./message-interface";

export interface IConversation {
    id: number;
    name: string;
    subname?: string | null;
    imageurl?: string | null;
    type: number;
    membercount?: number;
    ismuted?: boolean;
    isfavourite?: boolean;
    isread?: boolean;
    unreadcount?: number | null;
    members?: IMember[];
    messages?: IMessage [];
    candeletemessagesforallusers?: boolean;
    timecreated?: number;
}