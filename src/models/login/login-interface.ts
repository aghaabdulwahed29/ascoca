export interface ILogin {
    token: string;
    privatetoken: string;
    error: string;
    errorcode: string;
    stacktrace: string;
    debuginfo: string;
    reproductionlink: string;
}