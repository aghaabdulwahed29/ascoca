import { IUser } from "./user-interface";
import { IWarning } from "./warning-interface";

export interface ICredencial {
    users: IUser[];
    warnings: IWarning[];
}