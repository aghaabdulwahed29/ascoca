export interface IUser {
    id: number;
    username: string;
    firstname: string;
    lastname: string;
    fullname: string;
    email: string;
    department: string;
    firstaccess: number;
    lastaccess: number;
    auth: string;
    suspended: boolean;
    confirmed: boolean;
    lang: string;
    theme: string;
    timezone: string;
    mailformat: number;
    description: string;
    descriptionformat: number,
    country: string;
    profileimageurlsmall: string;
    profileimageurl: string;
    customfields: Array<{
        type: string;
        value: string;
        name: string;
        shortname: string;
    }>
}