import { IEvent } from "./event-interface";

export interface IHomeEvents {
    events: IEvent[];
    defaulteventcontext: number;
    filter_selector: string;
    courseid: number;
    categoryid: number;
    isloggedin: true;
    date: {
        seconds: number;
        minutes: number;
        hours: number;
        mday: number;
        wday: number;
        mon: number;
        year: number;
        yday: number;
        weekday: string;
        month: string;
        timestamp: number
    }
    
}