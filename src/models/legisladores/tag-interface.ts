export interface ITag {
    id: number;
    name: string;
    rawname: string;
    isstandard: false,
    tagcollid: number;
    taginstanceid: number;
    taginstancecontextid: number;
    itemid: number;
    ordering: number;
    flag: number;
}