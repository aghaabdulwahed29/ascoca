import { IAttachmentFile } from "./attachment-file-interface";
import { ITag } from "./tag-interface";

export interface IEntry {
    id: number;
    module: string;
    userid: number;
    courseid: number;
    groupid: number;
    moduleid: number;
    coursemoduleid: number;
    subject: string;
    summary: string;
    summaryformat: number;
    content: null;
    uniquehash: string;
    rating: number;
    format: number;
    attachment: string;
    publishstate: string;
    lastmodified: number;
    created: number;
    usermodified: null;
    summaryfiles: any[];
    attachmentfiles: IAttachmentFile [],
    tags: ITag []
}