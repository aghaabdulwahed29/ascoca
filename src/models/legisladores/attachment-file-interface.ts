export interface IAttachmentFile {
    filename: string;
    filepath: string;
    filesize: number;
    fileurl: string;
    timemodified: number;
    mimetype: string;
    isexternalfile: false;
}