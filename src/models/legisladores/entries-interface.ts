import { IEntry } from "./entry-interface";

export interface IEntries {
    entries: IEntry[];
    totalentries: number;
    warnings: any[];
}