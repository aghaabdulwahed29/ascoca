export interface IAttachment {
    contextid: number;
    component: string;
    filearea: string;
    itemid: number;
    filepath: string;
    filename: string;
    isdir: false;
    isimage: false;
    timemodified: number;
    timecreated: number;
    filesize: number;
    author: string;
    license: string;
    filenameshort: string;
    filesizeformatted: string;
    icon: string;
    timecreatedformatted: string;
    timemodifiedformatted: string;
    url: string;
    urls: {
        export: any;
    },
    html: {
        plagiarism: any;
    }
}