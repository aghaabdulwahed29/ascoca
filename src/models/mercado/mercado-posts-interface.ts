import { IRatinginfo } from "./ratinginfo-interface";
import { IPost } from "./post-interface";

export interface IMercadoPosts {
    posts: IPost[];
    forumid: number;
    courseid: number;
    ratinginfo: IRatinginfo;
    warnings: any [];
}