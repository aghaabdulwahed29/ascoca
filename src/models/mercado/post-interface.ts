import { IAttachment } from "./attachment-interface";
import { IAuthor } from "./author-interface";
import { ICapability } from "./capability-interface";
import { IHtml } from "./html-interface";
import { IUrl } from "./url-interface";

export interface IPost {
    id: number;
    subject: string;
    replysubject: string;
    message: string;
    messageformat: number;
    author: IAuthor,
    discussionid: number;
    hasparent: boolean;
    parentid: number | null;
    timecreated: number;
    timemodified: number;
    unread: any;
    isdeleted: boolean;
    isprivatereply: boolean;
    haswordcount: boolean;
    wordcount: any;
    charcount: any;
    capabilities: ICapability;
    urls: IUrl;
    attachments: IAttachment[];
    tags: any [];
    html: IHtml
}