import React from 'react';
import {Text, Image, TouchableOpacity} from 'react-native';

const HeaderMenu = () => ({
  ({onPress, ...params}) =>
    (
    <TouchableOpacity style={{marginRight: 20}} onPress={onPress}>
        <Image source ={'../assets/images/menu.png'}/>
        <Text style={{color="#C10202"}}>Menú</Text>
    </TouchableOpacity>
);

export default HeaderMenu;
