import React from 'react';
import {Image, StyleSheet, View} from 'react-native';

const HeaderView = () => (
  <View style={styles.container}>
    <Image
      source={require('../assets/images/logo.png')}
      style={styles.brand}
    />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  brand: {
    height: 48,
    resizeMode: 'contain',
    marginVertical: 10,
    marginRight:45,
  },
});

export default HeaderView;