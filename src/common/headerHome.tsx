import React from 'react';
import {Image, StyleSheet, View} from 'react-native';

const HeaderHome = () => (
  <View style={styles.container}>
    <Image
      source={require('../assets/images/logob.png')}
      style={styles.brand}
    />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  brand: {
    height: 30,
    resizeMode: 'contain',
    marginVertical: 10,
  },
});

export default HeaderHome;
