import React, {Component, useState} from 'react';
import {StyleSheet, Text, View, Image,TouchableOpacity,ScrollView} from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';
import { CredencialService } from '../../services/credencial/credencial-service';
import { dummyCredencial } from '../../utils/data';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
/*
const htmlContent =`
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div>

    <h3>Credencial Trastienda</h3>
    <div style="border-width: 1px;border-style: solid; border-color: grey;">
        <div style="background-color: rgb(206, 206, 206);">
            <p style="text-align: end;">Folio: ${credencial.id}</p>
        </div>
        <div>
            <div>
                <div style="display: flex; flex-wrap: wrap; justify-content: space-around;">
                    <div>
                        <h1>Foto</h1>
                    </div>
                    <div>
                        <h6>Admin User</h6>
                        <h3>"Abarrotes Gena"</h3>
                        <p>Vicente Guerrero #315, Coyoacán, CDMX, Sección 35</p>
                    </div>
                </div>
            </div>
            <div style="text-align: center;">
                <h1>QR</h1>
            </div>
        </div>
        <div style="background-color: rgb(206, 206, 206);">
            <p style="text-align: center;">vigencia:</p>
        </div>
    </div>
    </div>
</body>
</html>
`;
*/
const iD = (props: any) => {

  let credencialService = new CredencialService();
  const [credencial, setCredencial] = useState(dummyCredencial.users![0]);

  credencialService
    .getInfoCredencial('b6ebe494b06e55cb91a11cc3e06c586f', 'username', 'alumno3')
    .then(data => {
      setCredencial(data.users![0]);
      // console.log(data.events);
    })
    .catch(e => {
      console.error(e);
      //Alert.alert('No se pudieron cargar las conversaciones');
    });

    const htmlContent =`
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div>

    <h3>Credencial Trastienda</h3>
    <div style="border-width: 1px;border-style: solid; border-color: grey;">
        <div style="background-color: rgb(206, 206, 206);">
            <p style="text-align: end;">Folio: ${credencial.id}</p>
        </div>
        <div>
            <div>
                <div style="display: flex; flex-wrap: wrap; justify-content: space-around;">
                    <div>
                        <h1>Foto</h1>
                    </div>
                    <div>
                        <h6>${credencial.fullname}</h6>
                        <h3>${credencial.customfields![0].value}</h3>
                        <p>${credencial.customfields![1].value}</p>
                    </div>
                </div>
            </div>
            <div style="text-align: center;">
              <h1>QR</h1>
            </div>
        </div>
        <div style="background-color: rgb(206, 206, 206);">
            <p style="text-align: center;">vigencia:</p>
        </div>
    </div>
    </div>
</body>
</html>
`;

   const createPdf = async () => {
    let options = {
      html: htmlContent,
      fileName: 'test999000',
      directory: 'Download',
    };

    let file = await RNHTMLtoPDF.convert(options)
    // console.log(file.filePath);
    alert(file.filePath);
  }

  return (
    <ScrollView>
      <View style={styles.containerHeader}>
        <Text style={styles.txtHeader}>
          Credencial Trastienda
        </Text>
      </View>
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerContent}>
            <Image
              style={styles.avatar}
              source={{uri: credencial.profileimageurl}}
            />
            <Text style={styles.userInfo}> Folio: {credencial.id}</Text>
            <Text style={styles.name}>{credencial.fullname} </Text>
            <Text style={styles.userStore}>"{credencial.customfields![0].value}" </Text>
            <Text style={styles.userAdrss}>
            {credencial.customfields![1].value}
            </Text>
            <Image
              style={styles.avatar}
              source={require('../../assets/images/QR.png')}
            />
          </View>
        </View>
        <View style={styles.expDate}>
          <Text style={styles.txtExpDate}>Vigencia 06/22</Text>
        </View>
        <TouchableOpacity 
        style={styles.dwnBtn}
        onPress={ () =>  createPdf() }
        >
        <Text style={styles.txtButton}> Descargar Credencial </Text>
          
      </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  containerHeader: {
    backgroundColor: '#fff',
  },
  txtHeader: {
    textAlign: 'center',
    fontFamily:'Nunito-ExtraBold',
    fontSize: 34,
  },
  container: {
    padding: 30,
  },
  header: {
    backgroundColor: '#ffff',
  },
  headerContent: {
    padding: 30,
    alignItems: 'center',
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 10,
    borderWidth: 4,
    borderColor: 'white',
    marginBottom: 10,
  },
  name: {
    fontSize: 18,
    color: '#000000',
    fontWeight: '600',
    paddingVertical: 5,
  },
  userInfo: {
    fontSize: 18,
    color: '#6F7074',
    fontWeight: '600',
    paddingBottom: 5,
  },
  userStore: {
    fontSize: 28,
    color: '#000000',
    fontWeight: 'bold',
    paddingVertical: 5,
  },
  userAdrss: {
    textAlign: 'center',
    fontSize: 18,
    color: '#6F7074',
    paddingVertical: 12,
  },
  expDate: {
    backgroundColor: '#E1E1E1',
    paddingVertical: 10,
  },
  txtExpDate: {
    textAlign: 'center',
    color: '#6F7074'
  },
  dwnBtn: {
    backgroundColor: '#fff',
    width: '100%',
    borderColor: '#C10202',
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    marginTop: 10,
  },
  txtButton: {
    color: '#C10202',
    textAlign: 'center',
    fontSize: 18,
  },
});

export default iD;