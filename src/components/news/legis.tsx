import React, {useState} from 'react';
import {View, StyleSheet, Text, Image, FlatList, ScrollView} from 'react-native';
import Carousel from '../../constants/carouselN';
import { LegisladoresService } from '../../services/legisladores/legisladores-service';
import {dummyNews} from '../../utils/data';
import {
  Container,
  Card,
  UserInfo,
  UserImgWrapper,
  UserImg,
  UserInfoText,
  UserName,
  PostTime,
  MessageText,
  TextSection,
} from '../styles/newsStyles';

const News = [
  {
    id:'1',
    messageNew: 'Tipping point: Can Summit put personalized learning over the top?',
  },
  {
    id:'2',
    messageNew: 'Five questions lawmakers should ask Devos abput teacher effectiveness',
  },
  {
    id:'3',
    messageNew: 'Nato has our \'unshakeable commitment\', Pentagon chief vows' ,
  },
  {
    id:'4',
    messageNew: 'Kanye is not American enough to perform at the inauguration',
  },{
    id:'5',
    messageNew: 'Tipping point: Can Summit put personalized learning over the top?',
  },{
    id:'6',
    messageNew: 'Tipping point: Can Summit put personalized learning over the top?',
  },
];

const Legis = () => {
  let legisladoresService = new LegisladoresService();
  const [upcomingNews, setUpcomingNews] = useState(dummyNews);

  legisladoresService
  .postGetNewsMisLegisladores('b6ebe494b06e55cb91a11cc3e06c586f', 'tag', 'Mis%20legisladores')
  .then(data => {
    setUpcomingNews(data.entries.map(e => {
      return {
        name: e.subject,
        description: 'https://i.ibb.co/hYjK44F/anise-aroma-art-bazaar-277253.jpg',
        //description: e.attachmentfiles![0].fileurl,
        location:
          '',
        id: e.id,
      };
    }));
    // console.log(data.events);
  })
  .catch(e => {
    console.error('No se pudieron cargar los eventos', e);
    // Alert.alert('No se pudieron cargar los eventos');
  });

  return (
    <ScrollView>
      <View style={styles.containerHeader}>
        <Text style={styles.txtHeader}>
          Mis legisladores
        </Text>
      </View>
      <View style={styles.container}>
        <View>
          <Text style={styles.txtLastNews}>
            Últimas Noticias
          </Text>
          <View style={styles.containerNews}>
          <Carousel data={upcomingNews} />
          </View>
        </View>
      </View>
      <View> 
      <View style={styles.containerList}>
      <FlatList 
        data={News}
        keyExtractor={item=>item.id}
        renderItem={({item}) => (
          <Card>
            <TextSection>
              <View style={styles.containerListNews}>
                <View style={styles.containerTextNews}>
                  <MessageText>{item.messageNew}</MessageText>
                </View>
                <View>
                  <Image
                  style={styles.preview}
                  source={require('../../assets/images/QR.png')}
                  />
                </View>
              </View>
              </TextSection>
          </Card>
        )}
      />
      </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  containerHeader: {
    backgroundColor: '#fff',
    padding: 10,
  },
  txtHeader: {
    textAlign: 'center',
    fontFamily:'Nunito-ExtraBold',
    fontSize: 34,
  },
  container: {
    padding: 30,
  },
  containerList: {
    alignItems: 'center',
  },
  containerTextNews: {
    width: 210,
  },
  containerListNews: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  txtLastNews: {
    textAlign: 'left',
    fontFamily:'Nunito-ExtraBold',
    fontSize: 24,
  },
  preview: {
    width: 50,
    height: 50,
  },
  containerNews: {
    height: 240,
  },
});

export default Legis;
