import React, {useState} from 'react';
import {View, StyleSheet, Text, TouchableOpacity, Alert} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import Carousel from '../../constants/carousel';
import {CalendarService} from '../../services/calendar/calendar-service';
import {dummyEvents} from '../../utils/data';

const Home = (props: any) => {
  let calendarService = new CalendarService();
  const [upcomingEvents, setUpcomingEvents] = useState(dummyEvents);

  calendarService
    .getCalendarUpcomingView('b6ebe494b06e55cb91a11cc3e06c586f')
    .then(data => {
      setUpcomingEvents(data.events);
      // console.log(data.events);
    })
    .catch(e => {
      console.error('No se pudieron cargar los eventos', e);
      // Alert.alert('No se pudieron cargar los eventos');
    });

    const onPressChats = () => console.log('Ir a la vista de chats');

  return (
      <View style={styles.container}>
        <View style={styles.carousel}>
          <Carousel data={upcomingEvents} />
        </View>
        <View style={styles.body}>
          <View >
          <TouchableOpacity style={styles.buttonContainer} 
            onPress={() => props.navigation.navigate('Chats', {})}>
              <Icon name="message-square" size={30} color="#C10202" />
              <Text style={styles.btnText} >Chats</Text>
              <Icon name="chevron-right" size={20} color="#C10202" style={{paddingLeft:185}}/>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonContainer}
            onPress={() => props.navigation.navigate('News', {})}>
              <Icon name="calendar" size={30} color="#C10202" />
              <Text style={styles.btnText}>Noticias de Pequeño {"\n"} Comercio</Text>
              <Icon name="chevron-right" size={20} color="#C10202" style={{paddingLeft:35}}/>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonContainer}>
              <Icon name="award" size={30} color="#C10202" />
              <Text style={styles.btnText}>Capacitación</Text>
              <Icon name="chevron-right" size={20} color="#C10202" style={{paddingLeft:113}}/>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonContainer}>
              <Icon name="credit-card" size={30} color="#C10202" />
              <Text style={styles.btnText}>Beneficios</Text>
              <Icon name="chevron-right" size={20} color="#C10202" style={{paddingLeft:140}}/>
            </TouchableOpacity>
          </View>
        </View>
      </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5F6FA',
    marginHorizontal: 5,
    marginRight: 10,
  },
  body: {
    marginTop: 10,
    marginHorizontal:10,
    alignItems: 'center',
  },
  bodyContent: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 15,
  },
  names: {
    fontSize: 28,
    color: '#696969',
    fontWeight: '300',
  },
  buttonContainer: {
    marginTop: 10,
    height: 80,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 10,
    width: 320,
    borderRadius: 7,
    backgroundColor: '#ffff',
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginLeft:5
  },
  btnText: {
    fontSize: 20,
    color: '#000000',
    marginLeft: 10,
    marginTop: 2,
    marginBottom: 10,
    fontFamily:'Nunito-Bold'
  },
  carousel:{
    height:120,
  },
});

export default Home;
