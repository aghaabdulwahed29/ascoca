import React, {useState, useContext, useEffect} from 'react';
import {
    GiftedChat,
    Bubble,
    Send,
    SystemMessage,
} from 'react-native-gifted-chat';
import {ActivityIndicator, View, Text, Platform,
  PermissionsAndroid,
  Dimensions,
  Alert,
  KeyboardAvoidingView,
  StyleSheet} from 'react-native';
import { AudioRecorder, AudioUtils } from "react-native-audio";
import Sound from "react-native-sound";
import Ionicons from "react-native-vector-icons/Ionicons";
import {IconButton} from 'react-native-paper';
import {AuthContext} from '../navigation/AuthProvider';
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import messaging from '@react-native-firebase/messaging';
import useStatsBar from '../utils/useStatusBar';
import NavigationBar from "react-native-navbar";
import AsyncStorage from '@react-native-async-storage/async-storage';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';


function ChatScreen({ route, navigation }) {
    // audio and images
    const [startAudio, setStartAudio] = useState(false);
    const [hasPermission, setHasPermission] = useState(false);
    const [audioPath, setAudioPath] = useState(`${
      AudioUtils.DocumentDirectoryPath
      }/${messageIdGenerator}test.aac`);
    
    const [playAudio, setPlayAudio] = useState(false);
    const [fetchChats, setFetchChats] = useState(false);
    const [audioSettings, setAudioSettings] = useState({
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: "Low",
      AudioEncoding: "aac",
      MeteringEnabled: true,
      IncludeBase64: true,
      AudioEncodingBitRate: 32000
  });
    // -- audio and images
    const [userStorage, setUserStorage] = useState({id: '', email: '', token: ''});
    const [messages, setMessages] = useState([]);
    const {thread} = route.params;

    async function handleSend(messages) {
    
        firestore()
          .collection('THREADS')
          .doc(thread._id)
          .collection('MESSAGES')
          .add({
            text: messages[0].text,
            messageType: "message",
            image: "",
            audio: "",
            createdAt: new Date().getTime(),
            user: {
              _id: userStorage.id,
              email: userStorage.email,
            },
          });
    
        await firestore()
          .collection('THREADS')
          .doc(thread._id)
          .set(
            {
              latestMessage: {
                text,
                createdAt: new Date().getTime(),
              },
            },
            {merge: true},
          );
      }

    useEffect(() => {
        getUser();
        const messagesListener = firestore()
        .collection('THREADS')
        .doc(thread._id)
        .collection('MESSAGES')
        .orderBy('createdAt', 'desc')
        .onSnapshot(querySnapshot => {
            const messages = querySnapshot.docs.map(doc => {
            const firebaseData = doc.data();

            const data = {
                _id: doc.id,
                text: firebaseData.messageType === "message" ? firebaseData.text : "",
                image: firebaseData.messageType === "image" ? firebaseData.image : "",
                audio: firebaseData.messageType === "audio" ? firebaseData.audio : "",
                messageType: firebaseData.messageType,
                createdAt: new Date().getTime(),
                ...firebaseData,
            };

            if (!firebaseData.system) {
                data.user = {
                ...firebaseData.user,
                name: firebaseData.user.email,
                };
            }

            return data;
            });
            setMessages(messages);
        });

        checkPermission().then(async hasPermission => {
            setHasPermission(hasPermission);
            if (!hasPermission) return;
            await AudioRecorder.prepareRecordingAtPath(
                audioPath,
                audioSettings
            );
            AudioRecorder.onProgress = data => {
                console.log(data, "onProgress data");
            };
            AudioRecorder.onFinished = data => {
                console.log(data, "on finish");
            };
        });

        // Stop listening for updates whenever the component unmounts
        return () => messagesListener();
    }, [thread._id]);

    const checkPermission = async () => {
        if (Platform.OS !== "android") {
            return Promise.resolve(true);
        }
        const rationale = {
            title: "Microphone Permission",
            message:
                "AudioExample needs access to your microphone so you can record audio."
        };
        return PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
            rationale
        ).then(result => {
            console.log("Permission result:", result);
            return result === true || result === PermissionsAndroid.RESULTS.GRANTED;
        });
    };

    async function getUser() {
        let userLocal = await AsyncStorage.getItem('user');
        if (userLocal) setUserStorage(JSON.parse(userLocal));
      }
    
    const renderAudio = (props) => {
        return !props.currentMessage.audio ? (
            <View />
        ) : (
                <Ionicons
                    name="ios-play"
                    size={35}
                    color={playAudio ? "red" : "blue"}
                    style={{
                        left: 90,
                        position: "relative",
                        shadowColor: "#000",
                        shadowOffset: { width: 0, height: 0 },
                        shadowOpacity: 0.5,
                        backgroundColor: "transparent"
                    }}
                    onPress={() => {
                        setPlayAudio(true);
                        const sound = new Sound(props.currentMessage.audio, "", error => {
                            if (error) {
                                console.log("failed to load the sound", error);
                            }
                            setPlayAudio(false);
                            sound.play(success => {
                                console.log(success, "success play");
                                if (!success) {
                                    Alert.alert("There was an error playing this audio");
                                }
                            });
                        });
                    }}
                />
            );
    };

      function renderBubble(props) {
        return (
          <View>
                {/*renderAudio(props)*/}
                <Bubble
                  {...props}
                  wrapperStyle={{
                    right: {
                      backgroundColor: '#C10202',
                    },
                  }}
                  textStyle={{
                    right: {
                      color: '#fff',
                    },
                  }}
                />
            </View>
        );
      }

      const handleAudio = async () => {
        if (!startAudio) {
          setStartAudio(true);
          await AudioRecorder.startRecording();
        } else {
            setStartAudio(false);
            await AudioRecorder.stopRecording();
            const fileName = `${messageIdGenerator}.aac`;
            const file = {
                uri: Platform.OS === "ios" ? audioPath : `file://${audioPath}`,
                name: fileName,
                type: `audio/aac`
            };
            const options = {
                keyPrefix: AwsConfig.keyPrefix,
                bucket: AwsConfig.bucket,
                region: AwsConfig.region,
                accessKey: AwsConfig.accessKey,
                secretKey: AwsConfig.secretKey,
            };
            /*RNS3.put(file, options)
                .progress(event => {
                    console.log(`percent: ${event.percent}`);
                })
                .then(response => {
                    console.log(response, "response from rns3 audio");
                    if (response.status !== 201) {
                        alert("Something went wrong, and the audio was not uploaded.");
                        console.error(response.body);
                        return;
                    }
                    const message = {};
                    message._id = messageIdGenerator;
                    message.createdAt = Date.now();
                    message.user = {
                        _id: user._id,
                        name: `${user.firstName} ${user.lastName}`,
                        avatar: user.avatar
                    };
                    message.text = "";
                    message.audio = response.headers.Location;
                    message.messageType = "audio";

                    this.chatsFromFB.update({
                        messages: [message, ...messages]
                    });
                })
                .catch(err => {
                    console.log(err, "err from audio upload");
                });*/
        }
      };

    const messageIdGenerator = () => {
      // generates uuid.
      return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, c => {
          let r = (Math.random() * 16) | 0,
              v = c == "x" ? r : (r & 0x3) | 0x8;
          return v.toString(16);
      });
    }

    const handleAddPicture = () => {
      const options = {
          title: "Select Profile Pic",
          mediaType: "photo",
          takePhotoButtonTitle: "Take a Photo",
          maxWidth: 256,
          maxHeight: 256,
          allowsEditing: true,
          noData: true,
          durationLimit: 15,
          includeBase64: true
      };
      //launchCamera(options, response => {
      launchImageLibrary(options, response => {
          // console.log("Response = ", response);
          if (response.didCancel) {
              // do nothing
          } else if (response.errorCode) {
              // alert error
          } else {
              const asset = response.assets[0];
              
              let storageRef = storage().ref().child('images')
                              .child(asset.fileName);
              storageRef
              .putString(asset.base64.toString(), 'base64').then(function(snapshot) {
                console.log('Uploaded a base64 string!', snapshot);

                storageRef.getDownloadURL().then(url => {

                  console.log('download url ', url);
  
                  firestore()
                  .collection('THREADS')
                  .doc(thread._id)
                  .collection('MESSAGES')
                  .add({
                    text: "",
                    messageType: "image",
                    image: url,
                    audio: "",
                    createdAt: new Date().getTime(),
                    user: {
                      _id: userStorage.id,
                      email: userStorage.email,
                    },
                  });
                }).catch(e => {
                  console.error('cant get public url', e);
                });

              }).catch(error => {
                console.log('Ocurrio un error guardando el archivo ', error);
              });
          }
      });
  };

  const renderAndroidMicrophone = () => {
    if (Platform.OS === "android") {
        return (
            <Ionicons
                name="ios-mic"
                size={35}
                hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
                color={startAudio ? "red" : "black"}
                style={{
                    bottom: 50,
                    right: Dimensions.get("window").width / 2,
                    position: "absolute",
                    shadowColor: "#000",
                    shadowOffset: { width: 0, height: 0 },
                    shadowOpacity: 0.5,
                    zIndex: 2,
                    backgroundColor: "transparent"
                }}
                onPress={handleAudio}
            />
        );
    }
  }

      function renderLoading() {
        return (
          <View style={styles.loadingContainer}>
            <ActivityIndicator size="large" color="#C10202" />
          </View>
        );
      }
    
      function renderSend(props) {
        return (
          <Send {...props}>
            <View style={styles.sendingContainer}>
              <IconButton icon="send-circle" size={32} color="#C10202" />
            </View>
          </Send>
        );
      }
    
      function scrollToBottomComponent() {
        return (
          <View style={styles.bottomComponentContainer}>
            <IconButton icon="chevron-double-down" size={36} color="#C10202" />
          </View>
        );
      }
    
      function renderSystemMessage(props) {
        return (
          <SystemMessage
            {...props}
            wrapperStyle={styles.systemMessageWrapper}
            textStyle={styles.systemMessageText}
          />
        );
      }

      const rightButtonConfig = {
          title: 'Add photo',
          handler: () => handleAddPicture(),
      };
    
      return (
        <View style={{ flex: 1 }}>
          <NavigationBar
                    title={{ title: "chat" }}
                    rightButton={rightButtonConfig}
                />
          {renderAndroidMicrophone}
          <GiftedChat
            messages={messages}
            onSend={handleSend}
            user={{_id: userStorage.id}}
            placeholder="Type your message here..."
            alwaysShowSend
            showUserAvatar
            scrollToBottom
            renderBubble={renderBubble}
            messageIdGenerator={messageIdGenerator}
            renderActions={() => {
              if (Platform.OS === "ios") {
                  return (
                      <Ionicons
                          name="ios-mic"
                          size={35}
                          hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
                          color={startAudio ? "red" : "black"}
                          style={{
                              bottom: 50,
                              right: Dimensions.get("window").width / 2,
                              position: "absolute",
                              shadowColor: "#000",
                              shadowOffset: { width: 0, height: 0 },
                              shadowOpacity: 0.5,
                              zIndex: 2,
                              backgroundColor: "transparent"
                          }}
                          onPress={handleAudio}
                      />
                  );
              }
          }}
            renderLoading={renderLoading}
            renderSend={renderSend}
            scrollToBottomComponent={scrollToBottomComponent}
            renderSystemMessage={renderSystemMessage}
          />
          <KeyboardAvoidingView />
        </View>
      );
}

const styles = StyleSheet.create({
    loadingContainer: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    sendingContainer: {
      justifyContent: 'center',
      alignItems: 'center',
    },
    bottomComponentContainer: {
      justifyContent: 'center',
      alignItems: 'center',
    },
    systemMessageWrapper: {
      backgroundColor: '#C10202',
      borderRadius: 4,
      padding: 5,
    },
    systemMessageText: {
      fontSize: 14,
      color: '#fff',
      fontWeight: 'bold',
    },
  });
  

export default ChatScreen