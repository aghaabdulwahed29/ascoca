import React, {useState, useEffect} from 'react';
import {View, StyleSheet} from 'react-native';
import {IconButton, Title} from 'react-native-paper';
import firestore from '@react-native-firebase/firestore';
import FormInput from '../commons/FormInput';
import FormButton from '../commons/FormButton';
import useStatsBar from '../utils/useStatusBar';
import AsyncStorage from '@react-native-async-storage/async-storage';

function AddGroupScreen({ navigation }) {
    const [roomName, setRoomName] = useState('');
    const [user, setUser] = useState({id: '', email: '', token: ''});

    useEffect(() => {
        getUser();
      }, []);

    async function getUser() {
        let userLocal = await AsyncStorage.getItem('user');
        if (userLocal) setUser(JSON.parse(userLocal));
      }

    function handleButtonPress() {
    if (roomName.length > 0) {
        firestore()
        .collection('THREADS')
        .add({
            name: roomName,
            latestMessage: {
            text: `You have joined the room ${roomName}.`,
            createdAt: new Date().getTime(),
            },
        })
        .then(docRef => {
            docRef.collection('MESSAGES').add({
            text: `You have joined the room ${roomName}.`,
            createdAt: new Date().getTime(),
            system: true,
            });
            navigation.navigate('Groups Screen');
        });
    }
    }

    return (
        <View style={styles.rootContainer}>
          <View style={styles.closeButtonContainer}>
            <IconButton
              icon="close-circle"
              size={36}
              color="#6646ee"
              onPress={() => navigation.goBack()}
            />
          </View>
          <View style={styles.innerContainer}>
            <Title style={styles.title}>Create a new chat room</Title>
            <FormInput
              labelName="Room Name"
              value={roomName}
              onChangeText={text => setRoomName(text)}
              clearButtonMode="while-editing"
            />
            <FormButton
              title="Create"
              modeValue="contained"
              labelStyle={styles.buttonLabel}
              onPress={() => handleButtonPress()}
              disabled={roomName.length === 0}
            />
          </View>
        </View>
      );
}

const styles = StyleSheet.create({
    rootContainer: {
      flex: 1,
    },
    closeButtonContainer: {
      position: 'absolute',
      top: 30,
      right: 0,
      zIndex: 1,
    },
    innerContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    title: {
      fontSize: 24,
      marginBottom: 10,
    },
    buttonLabel: {
      fontSize: 22,
    },
  });
  

export default AddGroupScreen