import { ICourse } from "../../models/courses/course-interface";

export class CoursesService {

    basePathWs: string = 'https://repo.ingenia.com'

    constructor() {}

    public async getCourses(wstoken: string, userid: string): Promise<ICourse[]> {
        const options = {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Basic aW5nZW5pYTpkZXNhcnJvbGxv'
          })
        };
    
        if (wstoken === null || userid === null) {
          throw new Error('Parametros incompletos');
        }

        const moodlewsrestformat = 'json';
        const wsfunction = 'core_enrol_get_users_courses';
    
        const response: Response = await fetch(`${this.basePathWs}/ascoca_app/Code/moodle/webservice/rest/server.php?wstoken=${wstoken}&moodlewsrestformat=${moodlewsrestformat}&wsfunction=${wsfunction}&userid=${userid}`, options);
    
        if(!response.ok) {
          console.error('Respuesta de red OK pero respuesta HTTP no OK');
          throw new Error('Ocurrio un error inesperado');
        }
    
        return response.clone().json();
      }
}