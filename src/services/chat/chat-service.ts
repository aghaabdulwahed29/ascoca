import { IContacts } from "../../models/chat/contacts-interface";
import { IConversation } from "../../models/chat/conversation-interface";
import { IConversations } from "../../models/chat/conversations-interface";
import { IMessage } from "../../models/chat/message-interface";

export class ChatService {
    basePathWs: string = 'https://repo.ingenia.com';

    constructor() {}

    public async getConversationsByUserId(wstoken: string, userid: string): Promise<IConversations> {
        const options = {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Basic aW5nZW5pYTpkZXNhcnJvbGxv'
          })
        };
    
        if (wstoken === null || userid === null) {
          throw new Error('Parametros incompletos');
        }
        const moodlewsrestformat = 'json';
        const wsfunction = 'core_message_get_conversations';
    
        const response: Response = await fetch(`${this.basePathWs}/ascoca_app/Code/moodle/webservice/rest/server.php?wstoken=${wstoken}&moodlewsrestformat=${moodlewsrestformat}&wsfunction=${wsfunction}&userid=${userid}`, options);
    
        if(!response.ok) {
          console.error('Respuesta de red OK pero respuesta HTTP no OK');
          throw new Error('Ocurrio un error inesperado');
        }
    
        return response.clone().json();
      }

      public async getConversationsByUserIdAndConversationId(wstoken: string, userid: string, conversationid: string): Promise<IConversation> {
        const options = {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Basic aW5nZW5pYTpkZXNhcnJvbGxv'
          })
        };
    
        if (wstoken === null || userid === null) {
          throw new Error('Parametros incompletos');
        }
        const moodlewsrestformat = 'json';
        const wsfunction = 'core_message_get_conversation';
        const includecontactrequests = '1';
        const includeprivacyinfo = '1';
    
        const response: Response = await fetch(`${this.basePathWs}/ascoca_app/Code/moodle/webservice/rest/server.php?wstoken=${wstoken}&moodlewsrestformat=${moodlewsrestformat}&wsfunction=${wsfunction}&userid=${userid}&conversationid=${conversationid}&includecontactrequests=${includecontactrequests}&includeprivacyinfo=${includeprivacyinfo}`, options);
    
        if(!response.ok) {
          console.error('Respuesta de red OK pero respuesta HTTP no OK');
          throw new Error('Ocurrio un error inesperado');
        }
    
        return response.clone().json();
      }

      public async postSendMessageToUserId(wstoken: string, touserid: string, text: string): Promise<Array<IMessage>> {
        const options = {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Basic aW5nZW5pYTpkZXNhcnJvbGxv'
          })
        };
    
        if (wstoken === null || touserid === null) {
          throw new Error('Parametros incompletos');
        }

        const moodlewsrestformat = 'json';
        const wsfunction = 'core_message_send_instant_messages';
        const textformat = 2;
    
        const response: Response = await fetch(`${this.basePathWs}/ascoca_app/Code/moodle/webservice/rest/server.php?wstoken=${wstoken}&moodlewsrestformat=${moodlewsrestformat}&wsfunction=${wsfunction}&messages[0][touserid]=${touserid}&messages[0][textformat]=${textformat}&messages[0][text]=${text}`, options);
    
        if(!response.ok) {
          console.error('Respuesta de red OK pero respuesta HTTP no OK');
          throw new Error('Ocurrio un error inesperado');
        }
    
        return response.clone().json();
      }

      public async postSearchContactsByString(wstoken: string, userid: string, search: string): Promise<IContacts> {
        const options = {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Basic aW5nZW5pYTpkZXNhcnJvbGxv'
          })
        };
    
        if (wstoken === null || userid === null) {
          throw new Error('Parametros incompletos');
        }

        const moodlewsrestformat = 'json';
        const wsfunction = 'core_message_message_search_users';
    
        const response: Response = await fetch(`${this.basePathWs}/ascoca_app/Code/moodle/webservice/rest/server.php?wstoken=${wstoken}&moodlewsrestformat=${moodlewsrestformat}&wsfunction=${wsfunction}&userid=${userid}&search=${search}`, options);
    
        if(!response.ok) {
          console.error('Respuesta de red OK pero respuesta HTTP no OK');
          throw new Error('Ocurrio un error inesperado');
        }
    
        return response.clone().json();
      }
}