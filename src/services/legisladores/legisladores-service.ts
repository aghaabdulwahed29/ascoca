import { IEntries } from "../../models/legisladores/entries-interface";

export class LegisladoresService {

    basePathWs: string = 'https://www.redtrastiendaanpec.com'

    constructor() {}

    public async postGetNewsMisLegisladores(wstoken: string, name:string, value: string): Promise<IEntries> {
        const options = {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          headers: new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Basic aW5nZW5pYTpkZXNhcnJvbGxv'
          })
        };
    
        if (wstoken === null) {
          throw new Error('Parametros incompletos');
        }

        const moodlewsrestformat = 'json';
        const wsfunction = 'core_blog_get_entries';
    
        const response: Response = await fetch(`${this.basePathWs}/webservice/rest/server.php?wstoken=${wstoken}&moodlewsrestformat=${moodlewsrestformat}&wsfunction=${wsfunction}&filters[0][name]=${name}&filters[0][value]=${value}`, options);
    
        if(!response.ok) {
          console.error('Respuesta de red OK pero respuesta HTTP no OK');
          throw new Error('Ocurrio un error inesperado');
        }
    
        return response.clone().json();
      }
}